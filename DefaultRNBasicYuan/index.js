import {AppRegistry} from 'react-native';
import LostOfStyles from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => LostOfStyles);